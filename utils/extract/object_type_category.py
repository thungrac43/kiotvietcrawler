import csv
from utils.product_name_decode import Decode
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.storage.postgres import object_type_constructor
def read_csv():
    """Read file csv and output dict define
    """        
    with open('../ResourceCrawler/ftech/object_type.csv', newline='', encoding='utf-8') as csvfile:
        list_dict_full = [{code:object_type  for code,  object_type in row.items()}
            for row in csv.DictReader(csvfile, skipinitialspace=True)]
        list_code_object_type = {row['code']: row['object_type'] for row in list_dict_full}
        list_code_category = {row['code']: row['category'] for row in list_dict_full}
        print(list_code_object_type) 
        print(list_code_category) 
def insert_object_type():
    """insert object_type from file to db
    """    
    with open('../ResourceCrawler/ftech/object_type.csv', newline='', encoding='utf-8') as csvfile:
        list_dict_full = [{code:object_type  for code,  object_type in row.items()}
            for row in csv.DictReader(csvfile, skipinitialspace=True)]
        list_object_types = {row['object_type']: row['name'] for row in list_dict_full}
        success = fail = 0
        # print(list_object_types)
        for key in list_object_types:
            if not bool(key):
                continue
            object_type = {}
            object_type['object_type'] = key
            object_type['name'] = list_object_types[key]
            object_type['shop_id'] = '1059845080701224'
            inserted_id = object_type_constructor.insert_shop_object_type(object_type)
            if inserted_id > 0:
                success+=1
            else:
                fail+=1
        print(f'success {success}, fail {fail}')