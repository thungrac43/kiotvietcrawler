# extract from kiotviet API output
import json
from sale_bot_backend_lib.entity.branch import Branch
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.storage.postgres.branch_constructor import insert_branch
def read_json():
    """Read file json and output dict define
    """        
    with open('../ResourceCrawler/kiotviet/branch.json', encoding='utf-8') as f:
        data = json.load(f)
        total = 0
        inserted = 0
        for row in data['Data']:
            total +=1
            branch = Branch()
            branch.shop_id = '1059845080701224'
            branch.source_id = row['Id']
            branch.name = row['Name']
            branch.address = row['Address']
            branch.province = row['Province']
            branch.district = row['District']
            branch.contact_number = row['ContactNumber'] if 'ContactNumber' in row else None
            branch.email = row['Email'] if 'Email' in row else None
            branch.ward_name = row['WardName']
            id_inserted = insert_branch(branch)
            inserted +=1 if id_inserted >0 else inserted
        print(f'total {total}, inserted {inserted}')
                
        