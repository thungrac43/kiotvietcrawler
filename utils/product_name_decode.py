import sys
from unidecode import unidecode
# sys.path.append(".")
from typing import List
from sale_bot_backend_lib.entity.product import Product
import configVal

def Decode(name: str):
    """Decode image name, product title to list of product

    Args:
        name (str): name without extension

    Returns:
        List[Product]: [description]
    """    
    #region clean data
    remove_trail_zero = unidecode(name.replace(' ', '').split('(')[0]) # T131TEF-FK3JAREU+Áo(2) => T131TEF-FK3JAREU+Ao
    products_title = remove_trail_zero.split('+') # split product
    #endregion clean data
    #region parse data
    
    list_products = [] # init
    list_category = []
    list_object_type = []
    for product_title in products_title: # each product
        
        #region list product
        product = Product()
        len_title = len(product_title)
        if(len_title not in (13, 16, 18)): # check length not contains color and age, not age, full
            continue
        product.model['code'] = product_title # model, url unique sql
        #region length 13
        
        #region age_group
        age_group = product_title[0].upper() # K
        if age_group not in configVal.age_groups:
            continue
        product.attribute['age_group'] = configVal.age_groups[age_group] # Baby, Kid, Teen
        #endregion age_group
        #region itemid
        itemid = product_title[1:4] # 297
        if(itemid.isdigit()):
            product.attribute['sub_itemid'] = itemid # 011
            gender = product_title[1:2] # even: girl, odd: boy
            product.attribute['gender'] = int(gender) % 2
        else:
            continue
        #endregion itemid
        #region year
        year = product_title[4:6].upper() # EI
        if year not in configVal.years:
            continue
        product.attribute['year'] = configVal.years[year] # 2018-2020
        #endregion year

        #region season
        season = product_title[6:7].upper() # F, S
        if season not in configVal.seasons:
            continue
        product.attribute['season'] = configVal.seasons[season] # Xuân hè, Thu đông
        #endregion season

        #region product_level
        product_level = product_title[8:9].upper() # B, F, L, D
        if product_level not in configVal.product_levels:
            continue
        product.attribute['level'] = configVal.product_levels[product_level] #  B: Dòng sản phẩm cơ bản. L: Dòng sản phẩm cao cấp. 
        #endregion product_level
        
        #region fabric
        fabric = product_title[9:10].upper() # T, K
        if fabric in configVal.fabrics:
            product.attribute['fabric'] = configVal.fabrics[fabric] # Dệt thoi, Dệt kim
        #endregion fabric

        #region product_positon - dòng sản phẩm - áo
        product_positon = product_title[10:11].upper() # T, K
        if product_positon in configVal.product_positons: # ignore condition in
            product.attribute['positon'] = configVal.product_positons[product_positon] # Dệt thoi, Dệt kim
        #endregion product_positon 
                
        #region product_sub_positon - nhóm hàng: áo phông
        product_sub_positon = (str(product.attribute['gender']) + product_title[10:11] + product_title[11:12]).upper() # 11A
        if product_sub_positon in configVal.product_sub_positons: # ignore condition in
            product.attribute['sub_positon'] = configVal.product_sub_positons[product_sub_positon] # Dệt thoi, Dệt kim
        #endregion product_sub_positon 

        #region product_style
        product_style = (product_sub_positon + product_title[12:13]).upper() # 11AB
        if product_style in configVal.product_styles: # ignore condition in
            product.attribute['style'] = configVal.product_styles[product_style] # Dệt thoi, Dệt kim
        #endregion product_style 

        #endregion length 13
        
        #region color
        if(len(product_title) >= 16):
           
            color = (product_title[13:16]).upper() # DEN
            if color in configVal.colors: #option
                product.color = configVal.colors[color] # Đen
        #endregion color             
        #region size
        if(len(product_title) >= 18):
            size = (product_title[16:18]).upper() # DEN
            product.size = size
        #endregion size                             
        
        list_products.append(product)   
        #endregion list products
        
        
        #region category - object_type
        if product_sub_positon in configVal.categories and bool(configVal.categories[product_sub_positon]):
            list_category.append({'code': product_title
                                  , 'name': configVal.categories[product_sub_positon]
                                  , 'category_level': 0
                                  , 'shop_id': '1059845080701224'
                                  }) #(code: str, category_val: str, category_level: int, shop_id_val: str)
            
            
        if product_sub_positon in configVal.object_types and bool(configVal.object_types[product_sub_positon]):
            list_object_type.append({'code': product_title
                                , 'object_type': configVal.object_types[product_sub_positon]
                                , 'object_type_level': 0
                                , 'shop_id': '1059845080701224'
                                }) 
        
    return list_products, list_category, list_object_type
    #endregion parse data
