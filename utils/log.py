import logging, os
import datetime, pytz
utc_now = datetime.datetime.utcnow() # now
utc_now_tz = utc_now.replace(tzinfo=pytz.UTC) # add timezone
utc_now_tz_vn = utc_now_tz.astimezone(pytz.timezone("Asia/Jakarta")) # convert to tz GMT+7
log_file =  utc_now_tz_vn.strftime("%d%m%Y") + '.txt' # download file

# logging.basicConfig(
#     level=logging.NOTSET,
#     format="%(asctime)s - %(module)s - %(funcName)s [%(levelname)s] :%(lineno)d %(message)s",
#     handlers=[
#         logging.FileHandler(log_file),
#         logging.StreamHandler()
#     ]
# )




formatter = logging.Formatter('%(asctime)s - %(module)s - %(funcName)s [%(levelname)s] :%(lineno)d %(message)s')
fh = logging.handlers.TimedRotatingFileHandler(os.path.join('_LOG', 'LOG.log'), when='d', interval=1, encoding='utf-8', utc = True)
fh.namer = lambda name: name.replace(".log.", "_") + ".log" # add extension handle for backup file
fh.setFormatter(formatter)

ch = logging.StreamHandler()
ch.setFormatter(formatter)

logging.getLogger().setLevel(logging.NOTSET)
logging.getLogger().addHandler(ch) # root logger in console only

logging.getLogger('my_log').setLevel(logging.NOTSET)
logging.getLogger('my_log').addHandler(fh)
logging.getLogger('my_log').addHandler(ch)
logging.getLogger('my_log').propagate = False # not inherit from root, prevent duplicate

