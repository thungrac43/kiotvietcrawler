from pandas import DataFrame
import pandas as pd
from typing import Tuple, List

def compare_two_lists(list1: list, list2: list, 
                    #   on = None, 
                      append_indicator: bool = False, 
                    #   suffixes=('_old', '_new'),
                      get_dict: List[str] = ['left', 'both', 'right']) -> Tuple[List, List, List]:
    """
    Compare two lists and logs the difference.
    :param list1: first list.
    :param list2: second list.
    :return:     List of left, both, right
    eg: compare_list.compare_two_lists([{'id':1, 'name': 'a1'}], [{'id':2, 'name':'a2'}])
    """
    df1 = pd.DataFrame(list1)
    df2 = pd.DataFrame(list2)
    diff = df1.merge(df2, indicator=True, how='outer')
    
    df_left = diff[diff['_merge'] == 'left_only']
    print(f'\nThere are {len(df_left)} left_only:\n{df_left.head()}')
        
    df_both = diff[diff['_merge'] == 'both']
    print(f'\nThere are {len(df_both)} both:\n{df_both.head()}')    
        
    df_right = diff[diff['_merge'] == 'right_only']
    print(f'\nThere are {len(df_right)} right_only:\n{df_right.head()}')    
    
    
    if not append_indicator: #  remove indicator
        del df_left['_merge']
        del df_both['_merge']
        del df_right['_merge']
        
    len_element = len(df_left.columns) # number of element   
    #region extract data left
    left_only = []
    if 'left' in get_dict:
        for i in df_left.values: # each value
            element = {}
            for j in range(len_element):
                element[df_left.columns[j]] = i[j]
            left_only.append(element)
        
    #endregion

    #region extract data both
    both = []
    if 'both' in get_dict:
        for i in df_both.values: # each value
            element = {}
            for j in range(len_element):
                element[df_both.columns[j]] = i[j]
            both.append(element)
    
    #endregion
    
    #region extract data right
    right_only = []
    if 'right' in get_dict:
        for i in df_right.values: # each value
            element = {}
            for j in range(len_element):
                element[df_right.columns[j]] = i[j]
            right_only.append(element)
    
    #endregion
    
    
    return left_only, both, right_only

def merge_two_lists(list1: list, list2: list, how='inner', on=None, left_on=None, 
                    right_on=None, left_index=False, right_index=False, sort=False, 
                    suffixes=('_old', '_new'), copy=True, indicator=False, validate=None, filter_by:str = ''
                    ) -> List[dict]:
    """merge 2 list return old and new value

    Args:
        list1 (list): [list old]. eg: [{1: 10, 2: 20, 3: 30}]
        list2 (list): [list new]. eg: [{1: 10, 2: 20, 3: 40}]
        base_on (list): [list key mapping] [1,2]
        suffixes (tuple, optional): [description]. Defaults to ('_old', '_new'). [{1: 10, 2: 20, 3_old: 30, 3_new: 40}]
        eg: data = compare_list.merge_two_lists(list1=[{1: 10, 2: 20, 3: 30}, {1: 10, 2: 30, 3: 40}, {1: 10, 2: 0, 3: 10}]
        , list2= [{1: 10, 2: 20, 3: 30}, {1: 10, 2: 30, 3: 50}, {1: 10, 2: 10, 3: 0}] )
        
        how: _old value always in left list, _new value always in right list
            left: get only in left
            right: get only in right
            inner: get both if on equal
        
        from utils import compare_list
        data = compare_list.merge_two_lists(
                                            list1=[{1: 10, 2: 20, 3: 30}, {1: 10, 2: 30, 3: 40}, {1: 10, 2: 0, 3: 10}]
                                            , list2= [{1: 10, 2: 20, 3: 30}, {1: 10, 2: 30, 3: 50}, {1: 10, 2: 10, 3: 0}]
                                            , on=[1,2]
                                            , filter_by = 3
                                            )        
    Returns:
        List[dict]: [new list with old and new value]
    """

    df1 = pd.DataFrame(list1)
    df2 = pd.DataFrame(list2)
    
    
    #region extract data left
    left_only = []
    df_left = df1.merge(df2, 'left', on, left_on, right_on, left_index, right_index, sort, suffixes, copy, indicator, validate)
    df_left = df_left[pd.isna(df_left[str(filter_by) + suffixes[1]])] # check _new value is NaN value
    len_element = len(df_left.columns) # number of element 
    for i in df_left.values: # each value
        element = {}
        for j in range(len_element):
            element[df_left.columns[j]] = i[j]
        left_only.append(element)
    
    #endregion

    #region extract data both
    equal =  []
    update = []
    df_both = df1.merge(df2, 'inner', on, left_on, right_on, left_index, right_index, sort, suffixes, copy, indicator, validate)
    
    df_equal = df_both[df_both[str(filter_by) + suffixes[0]] ==  df_both[str(filter_by) + suffixes[1]]]
    df_update = df_both[df_both[str(filter_by) + suffixes[0]] !=  df_both[str(filter_by) + suffixes[1]]]
    
    len_element = len(df_equal.columns) # number of element 
    for i in df_equal.values: # each value
        element = {}
        for j in range(len_element):
            element[df_equal.columns[j]] = i[j]
        equal.append(element)
    len_element = len(df_update.columns) # number of element         
    for i in df_update.values: # each value
        element = {}
        for j in range(len_element):
            element[df_update.columns[j]] = i[j]
        update.append(element)   
    #endregion
    
    #region extract data right
    right_only = []
    df_right = df1.merge(df2, 'right', on, left_on, right_on, left_index, right_index, sort, suffixes, copy, indicator, validate)
    df_right = df_right[pd.isna(df_right[str(filter_by) + suffixes[0]])] # check _new value is NaN value
    len_element = len(df_right.columns) # number of element 
    for i in df_right.values: # each value
        element = {}
        for j in range(len_element):
            element[df_right.columns[j]] = i[j]
        right_only.append(element)
    
    #endregion
    return left_only, equal, update, right_only