import requests, logging, shutil
logger = logging.getLogger(__name__)
def post_response(url, data, cookies, headers, json = None ):
    """
    send post request
    :return: raw response
    data: url encode data
    json: json post data
    """
    try:
        r = requests.post(url, data=data,  json = json,  cookies = cookies, headers=headers)
        return r
    except Exception as ex:
        logger.error(ex)
    return None
def get_json(url, params, cookies, headers):
    """
    send get request
    :return: response as json data
    """
    try:
        r = requests.get(url, params, cookies=cookies, headers=headers)
        return r.json()
    except Exception as ex:
        logger.error(ex)
    return None

def download_file(url, local_path):
    result = requests.get(url, stream=True)
    local_file = open(local_path, 'wb')
    result.raw.decode_content = True
    shutil.copyfileobj(result.raw, local_file)