import csv, configVal, logging
from utils.product_name_decode import Decode
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
logger = logging.getLogger('my_log')
def read_csv_products(filePath):
    """Read file csv and output dict define
    """        
    with open(filePath, newline='', encoding='utf-8') as csvfile:
        countAllProducts = countEmptyCode = countWrongCode = countCorrectCode = countProductCorrectCode = countAvaiable = countAvaiableAndCorrect =  0
        data = csv.reader(csvfile)
        next(csvfile)
        listProducts = []
        listCateGories = []
        listObjectTypes = []
        # countAllProducts = len(list(data))
        for row in data:            
            countAllProducts +=1
            quantity = int(float(row[7].replace(',', ''))) # 1,259.0000
            if(quantity > 0): 
                countAvaiable +=1            
            list_products, list_category, list_object_type = Decode(row[2]) # Decode product, category, object_type          
            #region addition product value
            if(len(list_products) == 0): # not extract code
                countWrongCode +=1
                continue
            raw_category = row[1].split('>>')[0] #'Áo>>16DBG0159>>BE' => Áo
            list_category_correct = list(filter(lambda x: raw_category.lower() in configVal.category_correct[x], configVal.category_correct)) # Quan => Quần
            if len(list_category_correct) > 0:
                list_category.append(
                                    {'code': row[2]
                                  , 'name': list_category_correct[0]
                                  , 'category_level': 0
                                  , 'shop_id': '1059845080701224'
                                  })
            else:
                list_category.append(
                                    {'code': row[2]
                                  , 'name': raw_category
                                  , 'category_level': 0
                                  , 'shop_id': '1059845080701224'
                                  })             
            list_category =   list({v['name']:v for v in list_category}.values())
            countCorrectCode +=1
            countProductCorrectCode += len(list_products) # result: each product per code            
            for product in list_products:                
                product.name = row[3]
                product.brand = row[4]
                product.price = int(float(row[5].replace(',', ''))) # 1,259.0000
                product.final_price = product.price
                if('material' in product.attribute):
                    product['material'] = product.attribute['material']
                    del product.attribute['material']
                # product.quantity = quantity TODO: csv file export only product of one brand
                product.quantity = 0
                # product_converted = json_dict_converter(product, convertDict=True) # for csv output only
                product.shop_market_place_id = 3
                product.shop_id = '1059845080701224'
                product.source = 'kiotviet'
                product['categories'] = list_category
                product['object_types'] = list_object_type
                listProducts.append(product)              
                
            if(quantity > 0): 
                countAvaiableAndCorrect +=1  
            # keys = listProducts[0].keys()
            # with open("product.csv","w+", encoding='utf-8') as my_csv:
            #     dict_writer = csv.DictWriter(my_csv, delimiter=',',lineterminator='\n', fieldnames = keys)
            #     dict_writer.writeheader()
            #     dict_writer.writerows(listProducts)     
            #endregion addition product value
            if len(list_category) > 0:
                listCateGories.extend(list_category)
            if len(list_object_type) > 0:
                listObjectTypes.extend(list_object_type)
            
        print(f'countAllProducts {countAllProducts}, countEmptyCode {countEmptyCode}, countWrongCode {countWrongCode},\
                    countCorrectCode {countCorrectCode}, countProductCorrectCode {countProductCorrectCode} countAvaiable {countAvaiable}\
                    countAvaiableAndCorrect {countAvaiableAndCorrect}')  
        return listProducts, listCateGories, listObjectTypes
