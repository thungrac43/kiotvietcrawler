import os, pickle, logging, datetime, pytz, time, configVal, json
import pandas as pd
from typing import Tuple, List
from utils import nettool, product_name_decode, log
from utils.compare_list import compare_two_lists, merge_two_lists
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.storage.data_storage import product_storage, shop_storage, category_storage, \
object_type_storage, branch_storage
from sale_bot_backend_lib.storage.postgres import product_constructor, category_constructor, object_type_constructor
logger = logging.getLogger('my_log')
def GetMasterProduct(cookies, take: int, page: int) -> dict:
    return {}
#region authen    
def loginKiotViet() -> Tuple[bool, object]:
    """Login kiotviet, write session to file and return cookie

    Returns:
        [type]: bool, cookies
    """    
    data = u'Password=guest&RememberMe=true&RememberMe=false&ShowCaptcha=False&UserName=guest&fingerPrintKey=dc92e17af2e431f81e945bccffd2344f_Chrome_Desktop_Máy tính Windows&quan-ly=Quản lý'
    headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    ret = nettool.post_response("https://kscloset28.kiotviet.vn/login", data.encode('utf-8'), cookies=None, headers= headers) 
    if not bool(ret): # fail
        return False, None
    cookie = ret.cookies
    if 'ss-tok' not in cookie: # check set authen
        return False, None
    with open('cookie.txt', 'wb') as f: # write new cookie
        pickle.dump(cookie, f)
    return True, cookie
def checkKiotVietAuthen() -> Tuple[bool, object]:
    """Read cookie file and check for expired

    Returns:
        bool: [description]
    """    
    
    if not os.path.exists('cookie.txt'):
        return False, None
    cookies = None
    headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
            "Accept": "application/json, text/plain, */*"
        }    
    try:
        with open('cookie.txt', 'rb') as f:
            cookies =  pickle.load(f)  
    except Exception as e:
        logger.exception(str(e))  
        return False, None
    ret = nettool.get_json('https://kscloset28.kiotviet.vn/api/setting/account', None, cookies = cookies, headers=headers)
    if not bool(ret):
        return False, None
    if ('Id' in ret and ret['Id'] > 0):
        return True, cookies
    return False, None
#endregion authen

#region update product 
def exportProductList() -> List[dict]:
    
    """Export from kiotviet and gen dict product

    Returns:
        [type]: [description]
    """    
    #region authen
    isLogin, cookies = checkKiotVietAuthen() 
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
        print('login failed')
        return None
    else:
        print('login success')
    #endregion authen   
    
    #region force export file
    utc_now = datetime.datetime.utcnow() # now
    utc_now_tz = utc_now.replace(tzinfo=pytz.UTC) # add timezone
    utc_now_tz_vn = utc_now_tz.astimezone(pytz.timezone("Asia/Jakarta")) # convert to tz GMT+7
    file_name =  'DanhSachSanPham_KV' + utc_now_tz_vn.strftime("%d%m%Y-%H%M%S-%f")[:-3] # download file
    params = {
                "Type": "Product",
                "FileName": file_name,
                "Filters": {
                    "CategoryId": 0,
                    "AttributeFilter": "[]",
                    "ProductTypes": "",
                    "IsImei": 2,
                    "IsFormulas": 2,
                    "IsActive": True,
                    "AllowSale": None,
                    "IsBatchExpireControl": 2,
                    "ShelvesIds": "",
                    "TrademarkIds": "",
                    "StockoutDate": "alltime",
                    "supplierIds": ""
                },
                "Revision": None        
            }
    retry_number = 10
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Content-Type": "application/json;charset=UTF-8",
        "FingerPrintKey": "",
        "Accept": "application/json, text/plain, */*"
    }
    revision = None
    while retry_number > 0:
        retry_number -= 1
        result = nettool.post_response(url="https://kscloset28.kiotviet.vn/api/importexportfiles/exportfile", data = None, json=params, cookies= cookies, headers= headers)
        result = result.json()
        if result and 'Data' in result and 'Revision' in result["Data"]:
            revision = result['Data']['Revision']
            break
        time.sleep(1) # second
    if not bool(revision):
        return None
    #endregion
    
    csv_data, _get_success = getExportFile(file_name, revision, cookies)
    return csv_data
def standardize_product(products: List[dict]) -> Tuple[List[dict],List[dict],List[dict] ]:
    """reformat product info from list of product base on code, quantity...
    return listProducts, listCateGories, listObjectTypes
    """        
    countAllProducts = countEmptyCode = countWrongCode = countCorrectCode = countProductCorrectCode = countAvaiable = countAvaiableAndCorrect =  0
    listProducts = []
    listCateGories = []
    listObjectTypes = []
    # countAllProducts = len(list(data))
    for p in products:            
        countAllProducts +=1
        quantity = p['Tồn kho']
        if(quantity > 0): 
            countAvaiable +=1            
        list_products, list_category, list_object_type = product_name_decode.Decode(p['Mã hàng']) # Decode product, category, object_type          
        #region addition product value
        if(len(list_products) == 0): # not extract code
            countWrongCode +=1
            continue
        raw_category = p['Nhóm hàng(3 Cấp)'].split('>>')[0] #'Áo>>16DBG0159>>BE' => Áo
        list_category_correct = list(filter(lambda x: raw_category.lower() in configVal.category_correct[x], configVal.category_correct)) # Quan => Quần
        if len(list_category_correct) > 0:
            list_category.append(
                                {'code': p['Mã hàng']
                                , 'name': list_category_correct[0]
                                , 'category_level': 0
                                , 'shop_id': '1059845080701224'
                                })
        else:
            list_category.append(
                                {'code': p['Mã hàng']
                                , 'name': raw_category
                                , 'category_level': 0
                                , 'shop_id': '1059845080701224'
                                })             
        list_category =   list({v['name']:v for v in list_category}.values())
        countCorrectCode +=1
        countProductCorrectCode += len(list_products) # result: each product per code            
        for product in list_products:                
            product.name = p['Tên hàng']
            product.brand = p['Thương hiệu']
            product.price = p['Giá bán'] # 1,259.0000
            product.final_price = product.price
            if('material' in product.attribute):
                product['material'] = product.attribute['material']
                del product.attribute['material']
            # product.quantity = quantity TODO: csv file export only product of one brand
            product.quantity = 0
            # product_converted = json_dict_converter(product, convertDict=True) # for csv output only
            product.shop_market_place_id = 3
            product.shop_id = '1059845080701224'
            product.source = 'kiotviet'
            product['categories'] = list_category
            product['object_types'] = list_object_type
            listProducts.append(product)              
            
        if(quantity > 0): 
            countAvaiableAndCorrect +=1  
        # keys = listProducts[0].keys()
        # with open("product.csv","w+", encoding='utf-8') as my_csv:
        #     dict_writer = csv.DictWriter(my_csv, delimiter=',',lineterminator='\n', fieldnames = keys)
        #     dict_writer.writeheader()
        #     dict_writer.writerows(listProducts)     
        #endregion addition product value
        if len(list_category) > 0:
            listCateGories.extend(list_category)
        if len(list_object_type) > 0:
            listObjectTypes.extend(list_object_type)
            
    print(f'countAllProducts {countAllProducts}, countEmptyCode {countEmptyCode}, countWrongCode {countWrongCode},\
                countCorrectCode {countCorrectCode}, countProductCorrectCode {countProductCorrectCode} countAvaiable {countAvaiable}\
                countAvaiableAndCorrect {countAvaiableAndCorrect}')  
    return listProducts, listCateGories, listObjectTypes
    
def upsert_products(products:List[dict], shop_id:str):
    """[insert or] update product from csv file
    """    
    ########## for testing ##########  
    # import pandas as pd
    # df = pd.read_excel('../ResourceCrawler/test.xlsx').to_dict(orient='index') # type: [0: dict, 1: dict]
    # dict_value = []
    # for i in df:
    #     dict_value.append(df[i]) 
    # kiotviet.upsert_products(dict_value, '1059845080701224')
    ########## for testing ##########  
    #region csv reader
    ######### product ##########
    #region pre process product code
    
    #endregion
    
    #region insert product
    success = fail = 0
    exists_products = product_storage.action_get_product_by_code(shop_id = shop_id) # get all products in db   
    exists_products_codes = [i['model']['code'] for i in exists_products] # extract product code
    list_not_exists = [i for i in products if i['model']['code'] not in  exists_products_codes]
    
    for product in list_not_exists:
        inserted_id = product_constructor.insert_product(product.__dict__) # convert object to dict
        if (inserted_id > 0):
            success +=1
        else:
            fail +=1
            continue
        #region insert category
        # category_products = filter(lambda category: category['code'] == product.model['code'], list_categories) # find in list cat
        for category in product['categories']: # for and insert
            inserted_id = category_constructor.insert_kiotviet_product_category(category['code'], category['name'], category['category_level'], category['shop_id'])
        #endregion insert category
        #region insert object_type
        # object_type_products = filter(lambda object_type: object_type['code'] == product.model['code'], list_object_types) # find in list cat
        for object_type in product['object_types']:
            inserted_id = object_type_constructor.insert_kiotviet_product_object_type(object_type['code'], object_type['object_type'], object_type['shop_id'])
        #endregion insert object_type
    #endregion insert product    
   
    print(f'success {success}, fail {fail}')  
    
    ######### product ##########
     
    #endregion csv reader    
#endregion    

#region update product  branch quantity
def exportProductBranch() -> List[dict]:
    
    """Export from kiotviet and gen dict product branch quantity

    Returns:
        [type]: [description]
    """    
    #region authen
    isLogin, cookies = checkKiotVietAuthen() 
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
        print('login failed')
        return None
    else:
        print('login success')
    #endregion authen   
    
    #region force export file
    utc_now = datetime.datetime.utcnow() # now
    utc_now_tz = utc_now.replace(tzinfo=pytz.UTC) # add timezone
    utc_now_tz_vn = utc_now_tz.astimezone(pytz.timezone("Asia/Jakarta")) # convert to tz GMT+7
    file_name =  'BaoCaoXuatNhapTonChiTiet_KV' + utc_now_tz_vn.strftime("%d%m%Y-%H%M%S-%f")[:-3] # download file
    
    current_date = utc_now.strftime("%d/%m/%Y")
    current_time = utc_now.strftime("%H:%M:%S.%f")[:-3]
    previous_date_minus = (utc_now - datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    current_date_minus = utc_now.strftime("%Y-%m-%d")
    filter = {
                    "hasCustomerFilter": False,
                    "TypeDiscountSelected": "[]",
                    "BranchIds": "[]",
                    "TimeRange": "other",
                    "dateLabelInit": "",
                    "OnHandFilter": 0,
                    "IsMapping": False,
                    "IsCategory": False,
                    "ShowInActiveProduct": 2,
                    "noPageSize": True,
                    "TableIds": "[]",
                    "TempTableIds": "[]",
                    "FilterDataRefreshPromiseQueue": "[]",
                    "GetTableFromAllBranches": True,
                    "typeDate": "other",
                    "dateLable": f"{current_date} - {current_date}",
                    "timeStamp": f"{current_date}T{current_time}Z",
                    "StartDate": f"{previous_date_minus}T17:00:00.000Z",
                    "EndDate": f"{current_date_minus}T17:00:00.000Z"
                }
    
    params = {
                "Type": "ProductInOutStockDetailRecord",
                "FileName": file_name,
                "Filters": json.dumps(filter), # NOTE: text, not json
                "Revision": None
            }
    # params = '{"Type": "ProductInOutStockDetailRecord", "FileName": "BaoCaoXuatNhapTonChiTiet_KV29122020-112304-817", "Filters": "{"hasCustomerFilter": false, "TypeDiscountSelected": "[]", "BranchIds": "[]", "TimeRange": "other", "dateLabelInit": "Tu\u1ea7n n\u00e0y", "OnHandFilter": 0, "IsMapping": false, "IsCategory": false, "ShowInActiveProduct": 2, "noPageSize": true, "TableIds": "[]", "TempTableIds": "[]", "FilterDataRefreshPromiseQueue": "[]", "GetTableFromAllBranches": true, "typeDate": "other", "dateLable": "29/12/2020 - 29/12/2020", "timeStamp": "29/12/2020T04:23:04.817Z", "StartDate": "2020-12-28T17:00:00.000Z", "EndDate": "2020-12-29T17:00:00.000Z"}", "Revision": null}'
    retry_number = 10
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Content-Type": "application/json;charset=UTF-8",
        "FingerPrintKey": "",
        "Accept": "application/json, text/plain, */*"
    }
    # params2 = json.loads(params)
    revision = None
    while retry_number > 0:
        retry_number -= 1
        result = nettool.post_response(url="https://kscloset28.kiotviet.vn/api/importexportfiles/exportfile", data = None, json=params, cookies= cookies, headers= headers)
        result = result.json()
        if result and 'Data' in result and 'Revision' in result["Data"]:
            revision = result['Data']['Revision']
            break
        time.sleep(1) # second
    if not bool(revision):
        return None
    #endregion
    csv_data, _get_success = getExportFile(file_name, revision, cookies)
    return csv_data
def standardize_product_branch(product_branchs: List[dict]):
    """reformat product branch info from list of product branch xlsx base on code
    """
    csv_data =[]        
    countAllProducts = countWrongCode = countCorrectCode = 0
    countAllProducts = len(product_branchs)
    for row in product_branchs:
        code = row['Mã hàng']
        list_product, _,_ = product_name_decode.Decode(code)
        if len(list_product) > 0: # valid            
            csv_data.append(
                    {
                        'code': code, 
                        'branch':row['Chi nhánh'],
                        'quantity': row['Tồn đầu kì']
                    }
                )
            # count +=1
            # if(count > 10):
            #     break
            countCorrectCode +=1
        else:
            countWrongCode +=1
    logger.info(f'countAllProducts {countAllProducts}, countWrongCode {countWrongCode},\
                countCorrectCode {countCorrectCode}')           
    return csv_data
def upsert_product_branchs(product_branchs:List[dict], shop_id:str):
    """[insert or] update product from csv file
    """    
    ########## for testing ##########  
    # import pandas as pd
    # df = pd.read_excel('../ResourceCrawler/test.xlsx').to_dict(orient='index') # type: [0: dict, 1: dict]
    # dict_value = []
    # for i in df:
    #     dict_value.append(df[i]) 
    # kiotviet.upsert_products(dict_value, '1059845080701224')
    ########## for testing ##########  
    # just inserted
    #region csv reader
    ######### product ##########
    #region add to db
    
    #region compare
    #region get raw data
    inserted_success = inserted_fail  = updated_success = updated_fail  = removed_rows_success = removed_rows_fail = not_change_rows = 0
    db_data = product_storage.action_products_branchs(shop_id=shop_id) # base on product on db.
    db_data_minimum = []
    for db_product_branch in db_data: # update exists product branchs
        db_code = db_product_branch['model']['code']
        for row in db_product_branch['branchs']: # each row in product_branch table
            db_data_minimum.append({'code': db_code, 'branch': row['name'], 'quantity':row['quantity']})
            
    #endregion
    #region compare include quantity
    # db_only, both_db, csv_only = compare_two_lists(db_data_minimum, csv_data) # diff include quantity
    # not_change_rows = len(both_db)
    #endregion
    #region compare branch
    # db_code_branch = [{'code':i['code'], 'branch':i['branch'], 'quantity': 0.1} for i in db_only]
    # csv_code_branch = [{'code':i['code'], 'branch':i['branch'], 'quantity': 0.1} for i in csv_only]
    
    # db_code_branch_only, both_code_branch, csv_code_branch_only = compare_two_lists(db_code_branch, csv_code_branch) # diff product code, branch
    
    # must_delete_row = len(db_code_branch_only)
    # must_updated_row = len(both_code_branch)
    # must_inserted_row = len(csv_code_branch_only)
    
    #endregion
    
    

    #region curd rows
    delete_rows, equal_rows, updated_rows, inserted_rows = merge_two_lists(db_data_minimum, product_branchs, on=['code', 'branch'], filter_by='quantity')
    
    must_delete_row = len(delete_rows)
    not_change_rows = len(equal_rows)
    must_updated_row = len(updated_rows)
    must_inserted_row = len(inserted_rows)
    #endregion    
    
    logger.info(f'must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, must_inserted_row {must_inserted_row}, not_change_row {not_change_rows}')
    #region delete row
    for row in delete_rows:
        db_removed_rows  = branch_storage.remove_product_branchs(code = row['code'], branch_name = row['branch'])
        if  db_removed_rows > 0:
                removed_rows_success += db_removed_rows
                logger.info(f"remove rows with code {row['code']}, branch {row['branch']} with quantity {row['quantity_old']}")   
        else:
            removed_rows_fail +=1
    #endregion
    #region update row
    for row in updated_rows:
        count_update_row  = branch_storage.update_product_branch_quantity(code = row['code'], branch_name = row['branch'], quantity =row['quantity_new'] )
        if(count_update_row > 0):
            updated_success +=1
            logger.info(f"update {count_update_row} rows with code {row['code']}, branch {row['branch']}, quantity from {row['quantity_old']} to {row['quantity_new']}")
        else:
            updated_fail +=1
            logger.error(f"update fail, rows with code {row['code']}, branch {row['branch']}, quantity from {row['quantity_old']} to {row['quantity_new']}")
    #endregion
    #region insert row
    for row in inserted_rows:
        inserted_id = branch_storage.insert_kiotviet_product_branch_quantity(code = row['code'], branch_name = row['branch'], shop_id = shop_id, quantity = row['quantity_new'])
        if inserted_id >0 :
            inserted_success +=1
            logger.info(f"insert {inserted_id} rows with code {row['code']}, branch  {row['branch']}, quantity {row['quantity_new']}")
        else:
            inserted_fail +=1
            logger.error(f"insert fail, code {row['code']}, branch {row['branch']}, quantity {row['quantity_new']}")
    #endregion    
    #endregion

         
    logger.info(f'removed_rows_success {removed_rows_success} removed_rows_fail {removed_rows_fail}, \
          inserted_success {inserted_success}, inserted_fail {inserted_fail},\
          updated_success {updated_success}, updated_fail {updated_fail}, \
          must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, \
          {must_inserted_row}, not_change_row {not_change_rows}')
    #endregion   
#endregion    
#endregion


#region update product discount
def exportProductDiscount(shop_id: str, exclude_pricebook: List[int] = []) -> Tuple[List[dict], str, bool]:
    """Export from kiotviet and gen dict product discount

    Args:
        shop_id (str): [description]
        exclude_pricebook (List[int], optional): List exclude pricebook. Defaults to [].

    Returns:
        Tuple[List[dict], str, bool]: 
        List of excels row and name of discount program
        str: name of discount program
        bool: get discount from kiotviet success or error, ignore process when error
        
    """    
    #region authen
    isLogin, cookies = checkKiotVietAuthen() 
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
        print('login failed')
        return None, None, False
    else:
        print('login success')
    #endregion authen 
    
    #region Get all discount program
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Content-Type": "XMLHttpRequest",
        "Accept": "application/json, text/javascript, */*; q=0.01"
    }
    all_discount = []
    retry_number = 10
    while retry_number > 0:
        retry_number -= 1
        result = nettool.get_json(f"https://kscloset28.kiotviet.vn/api/pricebook/filter?includeAll=true&Includes=PriceBookBranches&Includes=PriceBookCustomerGroups&Includes=PriceBookUsers&Includes=PriceBookDependency&IncludeSortName=true&%24inlinecount=allpages", None, cookies, headers)
        if result and 'Data' not in result: # fail
            time.sleep(2)
            continue
        if result and 'Data' in result:
            if 'Total' not in result: # check for valid format
                time.sleep(2)
                continue
            else:
                all_discount = result['Data']
                break                
        time.sleep(3) # second
    if not bool(all_discount): # finally
        return None, None, False   
    #endregion  
    
    #region check for avaiable discount
    avaiable_discount = [i for i in all_discount if 'CompareCommodityDisplayType' in i and i['CompareCommodityDisplayType'] == 1]
    
    utc_now = datetime.datetime.utcnow() # now
    utc_now_tz = utc_now.replace(tzinfo=pytz.UTC) # add timezone
    utc_now_tz_vn = utc_now_tz.astimezone(pytz.timezone("Asia/Jakarta")) # convert to tz GMT+7
    branch_source_ids = []
    max_date_promotion = None # max promotion program.
    PriceBookIds = None
    Name = None # discount_name
    for discount in avaiable_discount:
        #region check expired
        if 'EndDate' not in discount:
            continue
        EndDate = discount['EndDate']
        EndDate = datetime.datetime.strptime(EndDate[:26], '%Y-%m-%dT%H:%M:%S.%f') # GMT + 7
        EndDate = EndDate.replace(tzinfo=pytz.timezone("Asia/Jakarta"))
        if (utc_now_tz_vn > EndDate): # expired
            continue
        
        #endregion
        
        #region check begin
        if 'StartDate' not in discount:
            continue
        StartDate = discount['StartDate']
        StartDate = datetime.datetime.strptime(StartDate[:26], '%Y-%m-%dT%H:%M:%S.%f') # GMT + 7
        StartDate = StartDate.replace(tzinfo=pytz.timezone("Asia/Jakarta"))
        if (utc_now_tz_vn < StartDate): # not begin
            continue        
        #endregion
		
		#region check branch Hà nội
        PriceBookBranches = discount['PriceBookBranches'] # discount branchs
        if bool(PriceBookBranches): # not all branch
            if not bool(branch_source_ids): # get all branchs in Hà Nội
                branchs = branch_storage.get_branchs('Hà Nội', shop_id)
                branch_source_ids = [b['source_id'] for b in branchs]
            BranchIds = [b['BranchId'] for b in PriceBookBranches] # branch source_id from kiotviet
            
            duplicate = set(branch_source_ids) & set (BranchIds)
            if len(duplicate) == 0: # not apply to at least a Hà Nội branch
                continue
		#endregion
  
        #region check first near promotion
        if  not max_date_promotion \
            or (EndDate < max_date_promotion and max_date_promotion) \
            and discount['Id'] not in exclude_pricebook:
            max_date_promotion = EndDate
            PriceBookIds = discount['Id']
            Name = discount['Name']
        #endregion
        
    #endregion
    
    #region force export file
    
    if not PriceBookIds:        
        return None, None, True
    
    file_name =  'BangGia_KV' + utc_now_tz_vn.strftime("%d%m%Y-%H%M%S-%f")[:-3] # download file
    filter = {
                    "__type": "KiotViet.Web.Api.ListPriceBookItems, KiotViet.Web.Api",
                    "PriceBookIds": f"[{PriceBookIds}]",
                    "$top": "10",
                }
    
    params = {
                "Type": "PriceBook",
                "FileName": file_name,
                "Filters": json.dumps(filter), # NOTE: text, not json
                "Revision": None
            }
    retry_number = 10
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Content-Type": "application/json;charset=UTF-8",
        "FingerPrintKey": "",
        "Accept": "application/json, text/plain, */*"
    }
    # params2 = json.loads(params)
    revision = None
    while retry_number > 0:
        retry_number -= 1
        result = nettool.post_response(url="https://kscloset28.kiotviet.vn/api/importexportfiles/exportfile", data = None, json=params, cookies= cookies, headers= headers)
        result = result.json()
        if result and 'Data' in result and 'Revision' in result["Data"]:
            revision = result['Data']['Revision']
            break
        time.sleep(1) # second
    if not bool(revision):
        return None, None, False
    #endregion
    csv_data, get_success = getExportFile(file_name, revision, cookies)
    if get_success and not bool(csv_data) and configVal.KIOT_VIET_RETRY_EMPTY_DISCOUNT: # empty list, retry
        exclude_pricebook.append(PriceBookIds)
        return exportProductDiscount(shop_id, exclude_pricebook)
    return csv_data, Name, get_success
def standardize_product_discount(product_discounts: List[dict], name: str):
    """reformat product discount info from list of product discount xlsx and name of discount
    """
    csv_data =[]        
    countAllProducts = countWrongCode = countCorrectCode = 0
    countAllProducts = len(product_discounts)
    for row in product_discounts:
        code = row['Mã hàng']
        list_product, _,_ = product_name_decode.Decode(code)
        if len(list_product) > 0: # valid   
            price = row['Giá chung']
            final_price =  row[name]
            csv_data.append(
                    {
                        'code': code,
                        'price' : price,
                        'final_price':final_price,
                        'discount_rate': int((price - final_price) / price * 100)
                    }
                )
            # count +=1
            # if(count > 10):
            #     break
            countCorrectCode +=1
        else:
            countWrongCode +=1
    logger.info(f'countAllProducts {countAllProducts}, countWrongCode {countWrongCode},\
                countCorrectCode {countCorrectCode}')           
    return csv_data
def upsert_product_discounts(product_discounts:List[dict], shop_id:str):
    """[insert or] update product from csv file
    """
    #region get raw data
    updated_success = updated_fail  = removed_rows_success = removed_rows_fail = not_change_rows = 0
    db_data = product_storage.action_get_product_by_code(shop_id=shop_id) # base on product on db.
    db_data_discount =  list(filter(lambda element: element['discount_rate'] > 0, db_data))
    db_data_minimum = []
    for row in db_data_discount: # update exists product branchs
        db_data_minimum.append(
                {
                    'code': row['model']['code'], 
                    'price': row['price'], 
                    'final_price':row['final_price'],
                    'discount_rate': row['discount_rate']
                }
        )
    #endregion
    #region curd rows
    delete_rows, equal_rows, updated_rows = compare_two_lists(db_data_minimum, product_discounts)
    
    must_delete_row = len(delete_rows)
    not_change_rows = len(equal_rows)
    must_updated_row = len(updated_rows)
    logger.info(f'must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, not_change_row {not_change_rows}')
    #region delete row
    for row in delete_rows:
        updated_id  = product_storage.update_product_discount(code = row['code'], price = row['price'], final_price =row['price'], discount_rate = 0, shop_id=shop_id)
        if(updated_id and updated_id > 0):
            removed_rows_success +=1
            logger.info(f"restore success product_id {updated_id} with code {row['code']}, price {row['price']}, final_price {row['price']}, discount_rate 0")
        else:
            removed_rows_fail +=1
            logger.error(f"restore fail 1 rows with code {row['code']}, price {row['price']}, final_price {row['final_price']}, discount_rate 0")
    #endregion
    #region update row
    for row in updated_rows:
        updated_id  = product_storage.update_product_discount(code = row['code'], price = row['price'], final_price =row['final_price'], discount_rate = row['discount_rate'], shop_id=shop_id)
        if(updated_id and updated_id > 0):
            updated_success +=1
            logger.info(f"update success product_id {updated_id} with code {row['code']}, price {row['price']}, final_price {row['final_price']}, discount_rate {row['discount_rate']}")
        else:
            updated_fail +=1
            logger.error(f"update fail 1 rows with code {row['code']}, price {row['price']}, final_price {row['final_price']}, discount_rate {row['discount_rate']}")
    #endregion
   
    #endregion

         
    logger.info(f'removed_rows_success {removed_rows_success} removed_rows_fail {removed_rows_fail}, \
          updated_success {updated_success}, updated_fail {updated_fail}, \
          must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, \
          not_change_row {not_change_rows}')
#endregion
def getExportFile(file_name: str, revision:str, cookies:object) ->     Tuple[List[dict], bool]:
    """Export from kiotviet and gen dict

    Returns:
        [type]: [description]
        dict: list xlsx
        bool: get export file or not
    """       
    #region check for export file success or not and export dict
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Content-Type": "application/json;charset=UTF-8",
        "FingerPrintKey": "",
        "Accept": "application/json, text/plain, */*"
    }
    remote_file_path = None
    retry_number = 50
    while retry_number > 0:
        retry_number -= 1
        result = nettool.get_json(f"https://kscloset28.kiotviet.vn/api/importexportfiles?Revision={revision}", None, cookies, headers)
        if result and 'Data' not in result: # fail
            time.sleep(2)
            continue
        if result and 'Data' in result:
            if 'FilePath' in result["Data"][0]:
                remote_file_path = result['Data'][0]['FilePath']
                break
            if 'Status' in result["Data"][0] and result['Data'][0]['Status'] > 2: # 1: processing, 2: success, 3: fail
                logger.error(f"getExportFile {result['Data'][0]['Message'] if 'Message' in result['Data'][0] else 'Loi'}")
                break
        time.sleep(3) # second
    if not bool(remote_file_path): # finally
        return None, False
    local_file_path = os.path.join('xlsx', file_name + '.xlsx')
    nettool.download_file(remote_file_path, local_file_path) # download
    if not os.path.exists(local_file_path):
        return None, False
    df = pd.read_excel(local_file_path).to_dict(orient='index') # type: [0: dict, 1: dict]
    dict_value = []
    for i in df:
        dict_value.append(df[i])
    return dict_value, True # using pandas so int -> float
    
    #endregion

def crawl(shop_id: str):
    """Crawl new or existing product in shop

    Args:
        shop_id (str): eg: 1059845080701224

    Returns:
        [type]: [description]
    """    
    #region check shop
    shop_market_place = shop_storage.get_shop_market_places(shop_id)  # get shop_info
    if len(shop_market_place) <= 0:
        return {'message': 'cannot get shop information'}    
    shop_place_id = shop_market_place[0]["shop_market_place_id"] # shopee id
    shop_market_place_id = shop_market_place[0]["id"] # id in db
    #endregion
    #region authen
    isLogin, cookies = checkKiotVietAuthen() 
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
    if not isLogin: # not authen
        isLogin, cookies =  loginKiotViet()
        print('login failed')
    else:
        print('login success')
    #endregion authen        
        
    #region crawl all    
    item_counting = 0
    take = pageSize = 10 # number of getting products in a request
    # skip = 0
    page = 0 # start page
    params = {
        "format": "json",
        "Includes": "ProductAttributes",
        "ForSummaryRow": "true",
        "CategoryId": 0,
        "AttributeFilter": '[]', #[] %5B%5D
        "ProductTypes": '',
        "IsImei":2,
        "IsFormulas":2,
        "IsActive": 'true',
        "AllowSale": '',
        "IsBatchExpireControl": 2,
        "ShelvesIds": '',
        "TrademarkIds": '',
        "StockoutDate": "alltime",
        "filter%5Blogic%5D": "and"
    }
    url_temp = "https://kscloset28.kiotviet.vn/api/branchs/138630/masterproducts" # NOTE: branchs???
    # products_db = product_storage.get_shop_products(shop_id)[0] # old all products on db
    # products_db = product_storage.action_get_products(shop_id = shop_id) # old all products on db TODO: level 4: uncomment
    products_db = []
    itemids_db = [p['itemid'] for p in products_db] # extract all itemid
    codes_db = []
    for p in products_db:
        if p['model'] and 'code' in p['model']:
            codes_db.append(p['model']['code'])
    # codes_db = [p['model']['code'] for p in products_db] # not exist code => exception
    # crawl and update products
    while True:
        page += 1 # next page
        logger.info("crawl page %s", str(page))
        try:
            params["take"] = take # 10
            params["pageSize"] = pageSize # 10
            params["page"] = page # 3 - change
            params["skip"] = (page - 1) * take # 20
            items = crawl_single_page(url_temp, cookies, params)  # get items của shop
            if not bool(items): # empty response
                logger.error('crawl_single_page empty response')
                break
            for item in items:
                if item["ProductId"] <= 0: # ignore 
                    continue
                item_counting += 1
                logger.info("crawling the %s item", str(item_counting))
                # if(item["itemid"] != 1576550176): 
                #     continue
                if configVal.CRAWL_ONLY_NOT_EXIST == 'true': # if crawl only item not exist in db
                    if item["ProductId"]  in itemids_db or item["Code"] in codes_db:
                        continue
                continue_crawl =  crawl_item(item, int(item["ProductId"]), shop_market_place_id, shop_place_id, shop_id, cookies)
                if(item["ProductId"] in itemids_db ): # TODO: level 5 check if itemid not exists -> update
                    itemids_db.remove(item["ProductId"]) # remove after crawl
                if(item["Code"] in codes_db ): # TODO: level 5 check if itemid not exists -> update
                    codes_db.remove(item["Code"]) # remove after crawl
            if len(items) < take: # over
                break
        except Exception as ex:
            logger.error(ex)
    # if( len(itemids_db) > 0 and CRAWL_ONLY_NOT_EXIST == 'false'): # crawl item exist in db, but not exist in search, for delete
    #     for item in itemids_db:
    #         crawl_item(item, item["ProductId"], shop_market_place_id, shop_place_id, shop_id, cookies)
            
    #endregion crawl all            
    return {}
def crawl_single_page(url: str, cookies: object, params: dict)-> List[dict]:
    """retry 10 times and return json response

    Args:
        url (str): [description]
        cookies (object): [description]
        params (dict): [description]

    Returns:
        List[dict]: List of dict products
    """    
    retry_number = 10
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Accept": "application/json, text/javascript, */*; q=0.01",
        "X-Requested-With": "XMLHttpRequest"
    }  
    while retry_number > 0:
        retry_number -= 1
        result = nettool.get_json(url, params, cookies, headers)
        if result and 'TotalProduct' in result and 'Data' in result:
            return result['Data']
    return {}
def crawl_item(item:dict, itemid: int, shop_market_place_id: str, market_place_id: str, shop_id: str, cookies: object) -> bool:
    """CURD item in db and crawl itemid branchs quantity

    Args:
        item (dict): item information from kiotviet
        itemid (str): itemid in kiotviet
        shop_market_place_id (str): [description]
        market_place_id (str): [description]
        shop_id (str): [description]
        cookies (object): cookie for crawl item branchs quantity
    Returns: 
        bool: continue crawl or not
    """  
    count_upserted = count_fail = 0
    #region CURD item
    if bool(item):
        product_is_upserted, continue_crawl = curdProductInfo(item, shop_market_place_id, shop_id)
        if product_is_upserted:
            count_upserted +=1
            logger.info(f'crawl_item upserted {count_upserted}')
        else:
            count_fail +=1
            logger.error(f'crawl_item fail upserted {count_fail}')
        if not continue_crawl:
            return continue_crawl
    #endregion 

    #region crawl product branchs
    curdProductBrands(itemid, cookies)
    #endregion
    
    #region process itemid if item is None 
    # TODO: level 4 later
    if not bool(item): # TODO: level 4 crawl by itemid in kiotviet
        logger.error("crawl by kiotviet product id")
        return False
    #endregion
def curdProductInfo(item: dict,shop_market_place_id: str, shop_id: str) -> Tuple[bool, bool]:
    """CURD product

    Args:
        item (dict): item information from kiotviet api
        {"GroupProductType":0,"UnitListStr":"","MasterCode":"SPC201216142050405","ProductId":20815890,"BatchExpire":"","BatchExpireCount":0,"Suppliers":[],"Id":20815890,"ProductType":2,"CategoryId":819650,"CategoryName":"Quần","CategoryNameTree":"Quần","isActive":true,"HasVariants":false,"VariantCount":0,"AllowsSale":true,"isDeleted":false,"Name":"QUẦN TREGGING BÉ GÁI","FullName":"QUẦN TREGGING BÉ GÁI","Code":"K234TEF-BK2AEXTT09","BasePrice":219000.0000,"Cost":0,"LatestPurchasePrice":0.0000,"OnHand":0,"OnOrder":0,"OnHandCompareMin":0,"OnHandCompareMax":0,"CompareOnHand":0,"CompareCost":0,"CompareBasePrice":0,"Weight":0,"Reserved":0,"ActualReserved":0,"MinQuantity":0,"MaxQuantity":0,"CustomId":0,"CustomValue":0,"ConversionValue":1,"IsFavourite":0,"IsLotSerialControl":false,"IsRewardPoint":true,"IsBatchExpireControl":false,"FormulaCount":0,"Barcode":"","TradeMarkId":0,"TradeMarkName":""}
    Returns:
        [type]: product is upserted or not (for counting only), continue crawl or not (for future, now is always true)
    """    
    code = item['Code']
    continue_crawl = True # check if product is new update or not, default is True, in case product code is not valid
    product_is_upserted = False
    list_products, list_category, list_object_type= product_name_decode.Decode(code)
    if len(list_products) == 0: # wrong code
        return product_is_upserted, continue_crawl # wrong code
    if(len(code) != 18): # not exact true full info
        return product_is_upserted, continue_crawl
    
    #region compare decode category vs kiotviet category
    lc = list(filter(lambda o: o['name'].lower() == item['CategoryName'].lower(), list_category))
    if len(lc) == 0:
        list_category.append({'code': code
                        , 'name': item['CategoryName']
                        , 'category_level': 0
                        , 'shop_id': shop_id
                        }) 
    #endregion
    
    existsProduct = product_storage.action_get_products2(code=code, shop_id=shop_id) # get only 1 product    
    upserted_id = -1 # update or inserted_id
    updated_product = {} # save all updated product info                
    updated_product['shop_id'] = shop_id
    #region update
    if len(existsProduct) > 0:
        #region check updated product info or not
        for e_product in existsProduct: # just 1 product
            updated_product['id'] = e_product['id']
            if 'model' not in e_product or 'code' not in e_product['model'] or e_product['model']['code'] != code: # check code
                continue
            if bool(e_product['itemid'])  and e_product['itemid'] != item['Id']: # check already set id in db                
                continue
            if not bool (e_product['itemid']):                
                updated_product['itemid'] = item['Id'] # update null itemid
            
            new_price = int(float(item['BasePrice']))
            if new_price != e_product['final_price']:
                updated_product['final_price'] = new_price
                updated_product['discount_rate'] = int((e_product['price'] - new_price) / e_product['price'] * 100)
            if len(updated_product > 2):  #id, shop_id is default
                upserted_id = product_storage.update_product(updated_product)
        #end region
    #endregion
    
    #region insert
    else:
        inserted_product = json_dict_converter(list_products[0])
        inserted_product['shop_market_place_id'] = shop_market_place_id
        inserted_product['shop_id'] = shop_id
        inserted_product['name'] = item['Name']
        inserted_product['model'] = {'code':code}
        inserted_product['url'] = ''
        inserted_product['price'] = int(float(item['BasePrice']))
        inserted_product['final_price'] = inserted_product['price']
        inserted_product['discount_rate'] = 0
        inserted_product['quantity'] = 0
        inserted_product['itemid'] = item['Id']
        inserted_product['source'] = 'kiotviet'
        upserted_id = product_storage.insert_product(inserted_product)
    #endregion 
    
    #region category and object_type
    if(upserted_id > 0):
        product_is_upserted = True
        #region insert category
        list_shop_categories = category_storage.get_categories(shop_id)
        for category in list_category:
            # category['shop_id'] = shop_id
            lc = list(filter(lambda o: o['name'].lower() == category['name'].lower(), list_shop_categories))
            if len(lc) > 0: # set id
                category['id'] = lc[0]['id']
            else:
                c_id = category_storage.insert_category(category)
                if c_id > 0:
                    category['id'] = c_id # inserted
                else:
                    list_category.remove(category) # remove    
        if len(list_category) > 0:
            category_storage.upsert_product_categories(upserted_id, list_category, shop_id)
        
        #endregion
        #region insert object_type            
        # get object_type id
        list_shop_object_types = object_type_storage.get_object_types(shop_id)
        for object_type in list_object_type:
            object_type['shop_id'] = shop_id
            lot = list(filter(lambda o: o['name'].lower() == object_type['name'].lower(), list_shop_object_types))
            if len(lot) > 0: # set id
                object_type['id'] = lot[0]['id']
            else:
                o_id = object_type_storage.insert_object_type(object_type)
                if o_id > 0:
                    object_type['id'] = o_id # inserted
                else:
                    list_object_type.remove(object_type) # remove
        if len(list_object_type) > 0:
            object_type_storage.update_product_object_types(upserted_id, list_object_type, False)
        #endregion
    #endregion
    return product_is_upserted, continue_crawl
def curdProductBrands(itemid: int, cookies: dict) ->  Tuple[bool, bool]:
    """Upsert product branchs info
    Args:
        itemid (int): [description]
        cookies (dict): [description]

    Returns:
        Tuple[bool, bool]: upsert success, continue crawl base on product branch quantity
    """    
 
    #region process branchs  
    # return False

    item_url = f"https://kscloset28.kiotviet.vn/api/products/{itemid}/onhandbybranch"
    params = {}
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0",
        "Accept": "application/json, text/plain, */*",
        "FingerPrintKey": ""
    }  
    retry_number = 10
    result = None
    while retry_number > 0:
        retry_number -= 1
        result = nettool.get_json(item_url, params, cookies, headers)
        if result:
            break
    if result is not None and "Total" in result and  'Data' in result and bool(result['Data']):  # has item and item is not null (exist)
        branchs = result["Data"]
        # for branch in branchs:
            
    else:
        logger.error(f"cannot get product branchs {itemid}")
    #endregion    
def process_item(item: dict, itemid: str, shop_market_place_id: str, market_place_id: str, db_shop_id):
    return False
    # """extract details of item

    # Args:
    #     item (dict): item details
    #     itemid (str): item id
    #     shop_market_place_id (str): id of shop in market website
    #     market_place_id (str): 
    #     db_shop_id ([type]): at the moment, facebook id of shop
    # """
    # # only add argument in db because update_product will update all element in product
    # product = {
    #     "brand": item["brand"].strip().title(),
    #     "shop_id": str(db_shop_id),
    #     "shop_market_place_id": market_place_id,
    #     "url": "https://shopee.vn/product/" + str(shop_market_place_id) + "/" + str(itemid),
    #     "name": item["name"],
    #     "price": item["price_before_discount"] / 100000,
    #     "discount_rate": 0 if item["raw_discount"] is None else item["raw_discount"],
    #     "final_price": item["price"] / 100000,
    #     "quantity": item["stock"],
    #     "description": item["description"],
    #     "model": None,
    #     "itemid":itemid,
    #     "shop_market_place_id_raw": shop_market_place_id,  
    #     "modelid": None,
    #     "source": "shopee",
    # }
    # if product["price"] == 0:
    #     product["price"] = product["final_price"]
    # attributes = {}
    
    # if item["attributes"] is not None: 
    #     for attribute in item["attributes"]:
    #         # match vietnamese attribute to english
    #         if "name" in attribute and "value" in attribute: # check exist
    #             name = attribute["name"].lower().strip()
    #             value = attribute["value"].strip().title() #initcap
    #             if name.lower() in config._ATTRIBUTE_NAME_CONVERTED_MAP: # {"is_pending_qc":false,"idx":0,"value":"No Brand","id":13799,"is_timestamp":false,"name":"Thương hiệu"}
    #                 name = config._ATTRIBUTE_NAME_CONVERTED_MAP.get(name) # 'thương hiệu': 'brand' => get brand 
    #             if name not in config.product_attributes_column: # ignore attribute in column
    #                 attributes[name] = value # set value to attribute
    #             elif name not in product: # if product attribute not set
    #                 product[name] = value
    # product["attribute"] = attributes
    # categories = []
    # for idx, category in enumerate(item["categories"]):
    #     categories.append(
    #         {"name": category["display_name"], "category_level": idx})
    # product["categories"] = categories
    # # Insert hoặc update product vào bảng product
    # upsert_product(product,  item['images'], item)

