# Read branch - quantity export from kiotviet
#
#

import csv, logging
from typing import List
from pandas import DataFrame
import pandas as pd
from utils.product_name_decode import Decode
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.storage.data_storage import product_storage, branch_storage
from sale_bot_backend_lib.storage.postgres import branch_constructor, product_constructor
from sale_bot_backend_lib.entity.branch import ProductBranchs
from utils.compare_list import compare_two_lists, merge_two_lists
logger = logging.getLogger('my_log')
def read_csv(filePath: str, shop_id: str = None):
    """Read file csv and output dict define
    """    
    #region read csv and parse to list dict code-branch-quantity
    list_dict_full = []
    with open(filePath, newline='', encoding='utf-8') as csvfile:
        list_dict_full = [{key:value  for key, value in row.items()}
            for row in csv.DictReader(csvfile, skipinitialspace=True)]
    #endregion
    #region verify code
    csv_data = [] # list of ProductBranchs
    count = 0
    for row in list_dict_full:
        code = row['Mã hàng']
        list_product, _,_ = Decode(code)
        if len(list_product) > 0: # valid            
            csv_data.append(
                    {
                        'code': row['Mã hàng'], 
                        'branch':row['Chi nhánh'],
                        'quantity': int(float(row['Tồn đầu kì']))
                    }
                )
            # count +=1
            # if(count > 10):
            #     break
    logger.info(f'valid {len(csv_data)} / total {len(list_dict_full)}')
    #endregion
    #region add to db
    
    #region compare
    #region get raw data
    inserted_success = inserted_fail  = updated_success = updated_fail  = removed_rows_success = removed_rows_fail = not_change_rows = 0
    db_data = product_storage.action_products_branchs(shop_id=shop_id) # base on product on db.
    db_data_minimum = []
    for db_product_branch in db_data: # update exists product branchs
        db_code = db_product_branch['model']['code']
        for row in db_product_branch['branchs']: # each row in product_branch table
            db_data_minimum.append({'code': db_code, 'branch': row['name'], 'quantity':row['quantity']})
            
    #endregion
    #region compare include quantity
    # db_only, both_db, csv_only = compare_two_lists(db_data_minimum, csv_data) # diff include quantity
    # not_change_rows = len(both_db)
    #endregion
    #region compare branch
    # db_code_branch = [{'code':i['code'], 'branch':i['branch'], 'quantity': 0.1} for i in db_only]
    # csv_code_branch = [{'code':i['code'], 'branch':i['branch'], 'quantity': 0.1} for i in csv_only]
    
    # db_code_branch_only, both_code_branch, csv_code_branch_only = compare_two_lists(db_code_branch, csv_code_branch) # diff product code, branch
    
    # must_delete_row = len(db_code_branch_only)
    # must_updated_row = len(both_code_branch)
    # must_inserted_row = len(csv_code_branch_only)
    
    #endregion
    
    

    #region curd rows
    delete_rows, equal_rows, updated_rows, inserted_rows = merge_two_lists(db_data_minimum, csv_data, on=['code', 'branch'], filter_by='quantity')
    
    must_delete_row = len(delete_rows)
    not_change_rows = len(equal_rows)
    must_updated_row = len(updated_rows)
    must_inserted_row = len(inserted_rows)
    #endregion    
    
    logger.info(f'must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, must_inserted_row {must_inserted_row}, not_change_row {not_change_rows}')
    #region delete row
    for row in delete_rows:
        db_removed_rows  = branch_storage.remove_product_branchs(code = row['code'], branch_name = row['branch'])
        if  db_removed_rows > 0:
                removed_rows_success += db_removed_rows
                logger.info(f"remove rows with code {row['code']}, branch {row['branch']} with quantity {row['quantity_old']}")   
        else:
            removed_rows_fail +=1
    #endregion
    #region update row
    for row in updated_rows:
        updated_rows  = branch_storage.update_product_branch_quantity(code = row['code'], branch_name = row['branch'], quantity =row['quantity_new'] )
        if(updated_rows > 0):
            updated_success +=1
            logger.info(f"update {updated_rows} rows with code {row['code']}, branch {row['branch']}, quantity from {row['quantity_old']} to {row['quantity_new']}")
        else:
            updated_fail +=1
            logger.error(f"update fail, rows with code {row['code']}, branch {row['branch']}, quantity from {row['quantity_old']} to {row['quantity_new']}")
    #endregion
    #region insert row
    for row in inserted_rows:
        inserted_id = branch_storage.insert_kiotviet_product_branch_quantity(code = row['code'], branch_name = row['branch'], shop_id = shop_id, quantity = row['quantity_new'])
        if inserted_id >0 :
            inserted_success +=1
            logger.info(f"insert {inserted_id} rows with code {row['code']}, branch  {row['branch']}, quantity {row['quantity_new']}")
        else:
            inserted_fail +=1
            logger.error(f"insert fail, code {row['code']}, branch {row['branch']}, quantity {row['quantity_new']}")
    #endregion    
    #endregion

         
    logger.info(f'removed_rows_success {removed_rows_success} removed_rows_fail {removed_rows_fail}, \
          inserted_success {inserted_success}, inserted_fail {inserted_fail},\
          updated_success {updated_success}, updated_fail {updated_fail}, \
          must_delete_row {must_delete_row}, must_updated_row {must_updated_row}, \
          {must_inserted_row}, not_change_row {not_change_rows}')
    #endregion           
