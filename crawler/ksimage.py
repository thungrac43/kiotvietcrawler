import os, csv, urllib.parse, shutil, logging, requests, copy, pickle
from PIL import Image
from unidecode import unidecode
from typing import Tuple, List
import configVal
from requests.auth import HTTPBasicAuth
# from utils import log
from utils.product_name_decode import Decode
from utils.compare_list import compare_two_lists
from sale_bot_backend_lib import config
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.utils.hash import md5
from sale_bot_backend_lib.utils.image import exists_image
from sale_bot_backend_lib.storage.cores import images_http
from sale_bot_backend_lib.storage.postgres import image_constructor
from sale_bot_backend_lib.storage.data_storage import image_storage, object_type_storage, product_storage
from sale_bot_backend_lib import entity
from sale_bot_backend_lib.utils.kafka_utils import publish_event
from sale_bot_backend_lib.config import  SERVER_URL, HTTP_USER, HTTP_PWD
directory = u'D:/Work/Kscloset/Drive/2109. Hình ảnh'

# directory = u'D:\\Work\\Kscloset\\tmp'
compress_directory = 'D:/Work/Kscloset/compress_images'
logger = logging.getLogger('my_log')
def scan_drive_images(shop_id: str, shop_name:str, minimal_scan:bool = False):
    """Scan google drive folder and update to exists products
    """
    inserted_success = inserted_fail = 0

    drive_data = process_drive_images(shop_id, shop_name, minimal_scan) # too slow
    if len(drive_data) == 0:
        logger.info(f'scan_drive_images already up to date shop {shop_name}')
        return
    db_data = product_storage.action_products_images(shop_id = shop_id)
    db_data_minimum = []
    for db_product_image in db_data: # update exists product images
        for row in db_product_image['images']: # each row in product_image table
            db_data_minimum.append({'product_id': row['product_id'], 'image_name': row['name']})
    
    #region curd rows
    _delete_rows, _equal_rows, inserted_rows = compare_two_lists(db_data_minimum, drive_data, get_dict=['right']) # compare to insert only
    
    must_delete_row = len(_delete_rows)
    not_change_rows = len(_equal_rows)
    must_inserted_row = len(inserted_rows)
    #endregion    
    
    logger.info(f'must_delete_row {must_delete_row}, must_inserted_row {must_inserted_row}, not_change_row {not_change_rows}')

    #region insert row
    for row in inserted_rows:
        inserted_id = update_product_images(row)
        if inserted_id >0 :
            inserted_success +=1
            logger.info(f"insert {inserted_id} rows with code {row['code']}, image {row['raw_image_full_local_path']}")
        else:
            inserted_fail +=1
            logger.error(f"insert fail, code {row['code']}, image {row['raw_image_full_local_path']}")
    #endregion    
    #endregion

         
    logger.info(f'must_delete_row {must_delete_row}, must_inserted_row {must_inserted_row}, not_change_row {not_change_rows} \
                inserted_success {inserted_success}, inserted_fail {inserted_fail}\
                ')
    #endregion  
    
def process_drive_images(shop_id: str, shop_name: str, minimal_scan:bool = False) -> List[dict]:
    """Scan google drive folder and assign to exists products

    Returns:
        List[dict]: List image - product mapping
    """    
    
    #region read raw image info and assign to minimum code
    success = fail  = 0
    list_images = scanImages() # verify image name and extract list minimal code, image in local disk
    md5_images_rd = md5_images_reader() # read md5 of old list image
    old_length = len(md5_images_rd)
    #endregion
    
    #region compress image and assign to exist products [full code] from minimal code
    processed_images = []  # list assign image
    if minimal_scan:
        _delete_rows, _equal_rows, inserted_rows = compare_two_lists(md5_images_rd, list_images) # compare to insert only
        list_images = inserted_rows
    for image in list_images: # process each new image list_images
        processed_image, newImage = compress_images(shop_name, image, md5_images_rd) # compress md5 and build image row value
        if bool(processed_image):
            if newImage: # new image then insert to old md5 list image
                md5_images_rd.append({'raw_image_hash':processed_image['raw_image_hash'], 'image_full_local_path': processed_image['raw_image_full_local_path']})
                success+=1
                #region build md5
                # exists_image = list(filter(lambda p: p['image_full_local_path'] == image['image_full_local_path'], md5_images_rd))
                # if len(exists_image) == 0:
                #     raw_image_hash = md5(image['image_full_local_path'])# new image_name
                #     md5_images.append({'raw_image_hash':raw_image_hash, 'image_full_local_path': image['image_full_local_path']})
                #endregion         
            all_products_with_code = product_storage.action_get_product_by_code(code=processed_image['code'], shop_id = shop_id) # read products with code
            for row in all_products_with_code: 
                image_clone = copy.deepcopy(processed_image) # clone image
                image_clone['code'] = row['model']['code'] # assign image with new code abd
                image_clone['product_id'] = row['id']  # assign product_id for insert
                processed_images.append(image_clone)
        else:
            fail +=1
    if(old_length != len(md5_images_rd)): # write if change
        md5_images_writer(md5_images_rd)	
    logger.info(f'new images {len(md5_images_rd) - old_length}, success_compress  {success}, fail_compress {fail}')
    return processed_images
    #endregion
    
    
def scanImages(directory=directory):
    countAllFiles =  countNotImage = countImages = wrongName = correctName = oneProduct = multiProduct = numberProducts = 0
    # extensions = []
    listImages = [] # list image correct and path
    for _path, _subdirs, files in os.walk(directory):
        for name in files:
            countAllFiles+=1
            nameExtension = os.path.splitext(name) # get filename with extension
            if len(nameExtension) > 1: # name.extension
                extension = nameExtension[1].lower()[1:] # get extension                
                # if(extension not in extensions):
                #     extensions.append(extension)
                if extension not in configVal.image_extension: #except not image
                    countNotImage +=1
                    continue
                countImages +=1 
                image_name = os.path.splitext(name)[0].upper()
                extension = os.path.splitext(name)[1].lower()
                listProducts,_,_ = Decode(image_name)
                if(len(listProducts) == 0): # wrong name
                    wrongName+=1
                    continue
                correctName +=1
                if (len(listProducts) ==1 ):
                    oneProduct +=1
                else:
                    multiProduct +=1
                numberProducts += len(listProducts)
                for product in listProducts: # append code
                    listImages.append(
                        {
                            'code': product.model['code'],
                            'extension': extension,
                            'image_name': image_name,
                            'image_full_local_path': os.path.join(_path,name)}
                        )
            else:
                logger.error('Error ' + name)
    logger.info(f'countAllFiles {countAllFiles}, countNotImage {countNotImage}, countImages {countImages}, wrongName {wrongName},\
           correctName {correctName}, oneProduct {oneProduct}, multiProduct {multiProduct}, numberProducts {numberProducts}')
    
    return listImages

    ######################## Write to csv ######################## 
    # with open("images.csv","w+", encoding='utf-8') as my_csv:
    #     dict_writer = csv.DictWriter(my_csv, delimiter=',',lineterminator='\n', fieldnames = listImages[0].keys())
    #     dict_writer.writeheader()
    #     dict_writer.writerows(listImages)               
    # print(extensions)
    
def compress_images(shop_name, d_image: dict, md5_images_rd: List[dict]) -> Tuple[dict, bool]:
    
    """Compress image and return info for:
    - upload http image server
    - insert into db
    - send kafka message
    eg:
    d_image: {'image_name': 'K596NIS-DK4AGSAO', 'image_full_local_path': D:\\Drive\\2609. Hình ảnh\\K596NIS-DK4AGSAO.jpg}
    ret: 
        {'image_name': md5(image_full_local_path), 'image_full_local_path': os.path.join(_path,name), 'code': ''}
        bool: new image or not
    """       
    retImage = {}
    newImage = False
    try: 
        raw_image_hash = None # find md5 image already scan
        exists_image = list(filter(lambda p: p['image_full_local_path'] == d_image['image_full_local_path'], md5_images_rd))
        if len(exists_image) > 0:
            raw_image_hash = exists_image[0]['raw_image_hash']
            # return retImage, newImage # process in local not process continue => new product to exists image???
        else:
            raw_image_hash = md5(d_image['image_full_local_path'])# new image_name
            newImage = True        
        #region process name
        image_folder = os.path.splitext(d_image['image_name'])[0] # B035NIF-FK1LCVAN+B075NIF-BK1JAXAN+................jpg => B035NIF-FK1LCVAN+B075NIF-BK1JAXAN+...............
        image_folder = unidecode(image_folder.replace(' ', '').replace('+', '_').replace('.', '').split('(')[0]) # T131TEF-FK3JAREU + abc(2)... => T131TEF-FK3JAREU_abc
        new_image_folder = os.path.join(compress_directory, shop_name ,image_folder)
        new_image_path = os.path.join(new_image_folder, raw_image_hash + '.jpg')
        if not os.path.exists(new_image_folder): # create folder for each product, model image if not exist
            os.makedirs(new_image_folder) # create multiple directory levels './images/3566/2560608931' => local
        #endregion process name
        
        #region compress
        # if not exists or last compress error then compress
        if  (
                not os.path.exists(new_image_path) # not convert success
                or os.stat(new_image_path).st_size == 0 # convert fail            
            ): 
                #region check compress conditon
                if  (
                        os.stat(d_image['image_full_local_path']).st_size > 1000000  # size < 1MB                
                        or d_image['extension'] != '.jpg'  # always convert to jpg
                    ):
                    # image = Image.open(d_image['image_full_local_path']).convert("RGB") => still error in pixel
                    image = read_image(d_image['image_full_local_path'])# fix png image
                    if not bool(image): # exception, return None
                        return retImage, newImage
                    compress_quality = 95 # max quality    
                    while compress_quality > 20: # limit quality
                        try:
                            image.save(new_image_path,quality=compress_quality,optimize=True)
                        except Exception as e:
                            logger.exception(str(e))    
                            return retImage, newImage
                        if( os.stat(new_image_path).st_size) > 1000000: # maximum size for upload to http server
                            compress_quality -=5
                            continue
                        break
                    if compress_quality < 20: # not compress success
                        if os.path.exists(new_image_path): # remove if exists
                            os.remove(new_image_path)
                        return retImage, newImage # return  
                #endregion
                #region or no need to compress, just copy
                else:
                    if  (
                        os.stat(d_image['image_full_local_path']).st_size <= 1000000 # size < 1MB
                        and d_image['extension'] == '.jpg' # and jpg
                        ):
                            shutil.copy2(d_image['image_full_local_path'], new_image_path) # copy metadata, permission https://stackoverflow.com/a/30359308
            
                #endregion
        #endregion compress
        #region standard_images
        
        if os.path.exists(new_image_path):
            retImage['code']= d_image['code']
            retImage['extension']= os.path.splitext(d_image['image_full_local_path'])[1].lower()[1:]
            retImage['image_folder']= image_folder
            retImage['folder_name']= f'{shop_name}/{image_folder}/'
            retImage['image_full_local_path'] = new_image_path # 'C:/images/K003EIS-BK1AACAM03/df1c1b2dc7b488b0f4133825538bc311.jpg' - file on local disk
            retImage['raw_image_full_local_path'] = d_image['image_full_local_path'] # 'D:/Work/Kscloset/Drive/2109. Hình ảnh/K003EIS-BK1AACAM03.jpg' - file on local disk
            retImage['db_image_path'] =  f'./{shop_name}/{image_folder}/{raw_image_hash}.jpg' # ./kscloset/K003EIS-BK1AACAM03/df1c1b2dc7b488b0f4133825538bc311.jpg
            retImage['http_image_url'] = f'{config.IMAGE_SUFFIX}{shop_name}/{urllib.parse.quote(image_folder)}/{raw_image_hash}.jpg' # url save file
            retImage['db_url'] = f'{shop_name}/{urllib.parse.quote(image_folder)}/{raw_image_hash}.jpg' # url save file
            retImage['image_name'] = raw_image_hash + ".jpg"# name df1c1b2dc7b488b0f4133825538bc311
            retImage['raw_image_hash'] = raw_image_hash # name df1c1b2dc7b488b0f4133825538bc311
        #endregion standard_images
        return retImage, newImage
    except Exception as e:
        logger.exception(str(e))     
        return retImage, newImage
def read_image(img_path):
    """read image and return pil_image as PIL.Image.open() return

    Args:
        img_path ([type]): [description]

    Returns:
        [type]: [description]
    """    
    import numpy
    from cv2 import cv2
    from PIL import Image
    #saving image into a white bg
    #windows
    try:
        if os.stat(img_path).st_size == 0: # check error
            return None
        stream = open(img_path, "rb")
        bytes = bytearray(stream.read())
        numpyarray = numpy.asarray(bytes, dtype=numpy.uint8)
        img = cv2.imdecode(numpyarray, cv2.IMREAD_UNCHANGED)    
        # linux img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
        img_split = cv2.split(img)
        if len(img_split) == 3:
            pil_image = Image.fromarray(img[:, :, ::-1])
        else:
            b,g,r, a = cv2.split(img)

            new_img  = cv2.merge((b, g, r))
            not_a = cv2.bitwise_not(a)
            not_a = cv2.cvtColor(not_a, cv2.COLOR_GRAY2BGR)
            new_img = cv2.bitwise_and(new_img,new_img,mask = a)
            new_img = cv2.add(new_img, not_a)
            pil_image = Image.fromarray(new_img[:,:,::-1])
        # pil_image.show()
        return pil_image 
    except Exception as e:
        logger.exception(str(e))     
        return None    
def update_product_images(d_image: dict) -> int:
    """[Update product d_image to http server, db and publish event to kafka]

    Args:
        d_image (dict): [description]
    """
    image_id = -1
    if not bool(d_image):
        logger.error(f"empty image info")
        return image_id
    variation_id = None
    # delete and rewrite image for correction => run 1 time when error
    # if exists_image(d_image['http_image_url']) and d_image['extension'] == 'png': # fix upload png image
    #     # requests.delete(d_image['http_image_url'], auth=HTTPBasicAuth(username=HTTP_USER, password=HTTP_PWD))
    #     if not images_http.upload_images([d_image]): # then upload
    #         logger.error(f"uploaded new image {d_image['image_name']} fail")
    #         return False
    if not exists_image(d_image['http_image_url']): # check if image exists in our server
        if not images_http.upload_images([d_image]): # then upload
            logger.error(f"uploaded new image {d_image['image_name']} fail")
            return image_id
    img = entity.Image()
    img.name = d_image['image_name'] 
    img.path = d_image['db_image_path']
    img.url = d_image['db_image_path'][2:]
    image_type = 0 # product image
    if variation_id: # variation image
        image_type = 1
    if 'shopee_feedback' in img.url: # feedback image
        image_type = 2
    image_id = image_constructor.insert_product_image(d_image['product_id'], img.name, img.path,
                                                        img.url, variation_id)
    if image_id and image_id > 0: # if inserted
        logger.info(f"inserted new image to db with id {image_id}")
        object_types = object_type_storage.get_product_object_types(d_image['product_id'])
        object_types_arr = [o["object_type"] for o in object_types]
        img_cv = {}
        img_cv["id"] = image_id
        img_cv["url"] = img.url                   
        img_cv["product_id"] = d_image['product_id']
        img_cv["variation_id"] = variation_id
        img_cv["object_type"] = object_types_arr
        img_cv["image_type"] = image_type
        img_cv['product_code'] = d_image['code']
        publish_event(config.CV_TOPIC, config.CV_EVENT_INSERTED, img_cv)
    return image_id
def read_png_image(img_path):
    """read image and return pil_image as PIL.Image.open() return

    Args:
        img_path ([type]): [description]

    Returns:
        [type]: [description]
    """    
    from cv2 import cv2
    import  numpy
    from PIL import Image
    #saving image into a white bg
    #windows
    stream = open(img_path, "rb")
    bytes = bytearray(stream.read())
    numpyarray = numpy.asarray(bytes, dtype=numpy.uint8)
    img = cv2.imdecode(numpyarray, cv2.IMREAD_UNCHANGED)    
    # linux img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
    img_split = cv2.split(img)
    if len(img_split) == 3:
        pil_image = Image.fromarray(img[:, :, ::-1])
    else:
        b,g,r, a = cv2.split(img)

        new_img  = cv2.merge((b, g, r))
        not_a = cv2.bitwise_not(a)
        not_a = cv2.cvtColor(not_a, cv2.COLOR_GRAY2BGR)
        new_img = cv2.bitwise_and(new_img,new_img,mask = a)
        new_img = cv2.add(new_img, not_a)
        pil_image = Image.fromarray(new_img[:,:,::-1])
    # pil_image.show()
    return pil_image 
def md5_images_reader():
    """Build [{md5, path}] of valid image files in kscloset drive

    Returns:
        [type]: [description]
    """    
    md5images = []      
    try:
        if os.path.exists('md5images.txt'): 
            with open('md5images.txt', 'rb') as f:
                md5images =  pickle.load(f)  
    except Exception as e:
        logger.exception(str(e))    
    return md5images
def md5_images_writer(md5images: List[object]) -> bool:
    """read [{md5, path}] of valid image files in kscloset drive

    Returns:
        [type]: [description]
    """    
    try:
        with open('md5images.txt', 'wb') as f: # write new cookie
            pickle.dump(md5images, f)
            return True
    except Exception as e:
        logger.exception(str(e))  
        return False          