import time, os, csv, logging, json, pytz, datetime
import configVal
from typing import List
from PIL import Image
from crawler import csvreader, ksimage, cr_branch, kiotviet
from utils import nettool, log
from utils.product_name_decode import Decode
from sale_bot_backend_lib.storage.data_storage.storage import cache
from utils.extract import object_type_category, branch
from sale_bot_backend_lib.entity.product import ProductObjectTypes
from sale_bot_backend_lib.storage.data_storage import product_storage, size_storage, user_storage
from sale_bot_backend_lib.storage.postgres import product_constructor, category_constructor, object_type_constructor, size_constructor, user_constructor
from sale_bot_backend_lib.entity.entity import _dict_to_obj
from sale_bot_backend_lib.utils.common_utils import json_dict_converter
from sale_bot_backend_lib.utils.hash import md5
from sale_bot_backend_lib.storage.postgres import postgres

logger = logging.getLogger('my_log')

def upsert_product(shop_id:str) -> bool:
    """update shop products information via csv export file

    Args:
        shop_id (str): [description]

    Returns:
        bool: success or fail
    """
     
    #list_products, _list_categories, _list_object_types = csvreader.read_csv_products(filePath)   #from csv   
    list_xlsx = kiotviet.exportProductList() #from kiotviet
    if not bool(list_xlsx): # TODO level 5 retry
        logger.error('upsert_product: crawl data fail')
        return False
    list_products, _, _ = kiotviet.standardize_product(list_xlsx)
    if not bool(list_products):
        logger.error('upsert_product: empty list_products')
        return False  
    kiotviet.upsert_products(list_products, shop_id) # process list
    return True
def update_product_quantity(shop_id: str = None) -> bool:
    """crawl and update product quantity

    Args:
        shop_id (str, optional): [description]. Defaults to None.

    Returns:
        bool: [description]
    """    
    # import pandas as pd
    # df = pd.read_excel(os.path.join('output', 'BaoCaoXuatNhapTonChiTiet_KV29122020-132808-098.xlsx')).to_dict(orient='index') # type: [0: dict, 1: dict]
    # list_xlsx = []
    # for i in df:
    #     list_xlsx.append(df[i])
    # shop_id = '1059845080701224'
    # list_product_branchs = kiotviet.standardize_product_branch(list_xlsx)
    # if bool(list_product_branchs):
    #     kiotviet.upsert_product_branchs(list_product_branchs, shop_id) # process list    
    
    list_xlsx = kiotviet.exportProductBranch() #from kiotviet
    if not bool(list_xlsx): # TODO level 5 retry
        logger.error('update_product_quantity: crawl data fail')
        return False
    list_product_branchs = kiotviet.standardize_product_branch(list_xlsx)
    if not bool(list_product_branchs):
        logger.error('update_product_quantity: empty list_product_branchs')
        return False    
    kiotviet.upsert_product_branchs(list_product_branchs, shop_id) # process list
    return True

def update_product_discount(shop_id: str = None) -> bool:
    """crawl and update product discount

    Args:
        shop_id (str, optional): [description]. Defaults to None.

    Returns:
        bool: [description]
    """   
    list_xlsx, name, get_success = kiotviet.exportProductDiscount(shop_id) #from kiotviet
    if not get_success:
        logger.error(f'Not get promotion success {shop_id}')
        return False        
    if not bool(list_xlsx): # no discount
        updated_products = product_storage.reset_discount_rate_of_shop(shop_id)
        logger.info(f'No promotion avaiable, reset discount_rate and price of shop {shop_id}, numbers {updated_products}')
        return False
    list_product_discounts = kiotviet.standardize_product_discount(list_xlsx, name)
    if not bool(list_product_discounts):
        logger.error('update_product_discount: empty list_product_discounts')
        return False    
    kiotviet.upsert_product_discounts(list_product_discounts, shop_id) # process list
    return True

def action_get_available_branchs():
    query = 'SELECT * from action_get_available_branchs(%s)'
    data = postgres.PostgresConnector().execute_select(query, [[ 33698,33697]] , ProductObjectTypes()) # , "{1059845080701224}"
    print(len(data))
    values = json_dict_converter(data[0])
    print(values)   
def action_get_products():
    query = 'SELECT * from action_get_products(%s, %s)'
    data = postgres.PostgresConnector().execute_select(query, [None, '%K003EIS-BK1AACAM%', '1059845080701224'], ProductObjectTypes())
    print(len(data))
    values = json_dict_converter(data[0])
    print(values)
    # data = product_storage.action_get_products(id=33697)
    # values = json_dict_converter(data[0])
    # print(values)  

def delete_cache():
    cache.delete_key_pattern('get_shop_products_SI_*')
    cache.delete_key_pattern('search_products_SI_*')    
if __name__ == '__main__':
    shop_id = '1059845080701224'
    logger.info("---------- BEGIN SCAN ----------")
    start_time = time.time() 
    upsert_product(shop_id)
    logger.info("upsert_product: --- %s seconds ---" % (time.time() - start_time))
    start_time = time.time() 
    update_product_quantity(shop_id)
    logger.info("update_product_quantity: --- %s seconds ---" % (time.time() - start_time))
    start_time = time.time()
    ksimage.scan_drive_images(shop_id, 'kscloset', False)
    logger.info("scan_drive_images: --- %s seconds ---" % (time.time() - start_time))
    start_time = time.time()
    update_product_discount(shop_id)
    logger.info("update_product_discount: --- %s seconds ---" % (time.time() - start_time))